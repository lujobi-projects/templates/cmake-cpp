# {{ cookiecutter.project_name }}

## Content
This is a boilerplate for a C++ project.

## Dependencies
* clang/gcc
* clangd
* cmake
* cppcheck
* clang-format
* clang-tidy

## Credits
Michele Adduci <adduci@tutanota.com>
